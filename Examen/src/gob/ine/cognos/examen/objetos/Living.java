package gob.ine.cognos.examen.objetos;

public class Living extends Mueble {

	public String esquineros;

	public Living() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Living(String codigo, String tipo_madera, double precio) {
		super(codigo, tipo_madera, precio);
		// TODO Auto-generated constructor stub
	}

	public Living(int tipo,String codigo, String tipo_madera, double precio, String esquineros) {
		super(tipo,codigo, tipo_madera, precio);
		// TODO Auto-generated constructor stub
		this.esquineros = esquineros;
	}

	public Living(String esquineros) {
		super();
		this.esquineros = esquineros;
	}

	public String getEsquineros() {
		return esquineros;
	}

	public void setEsquineros(String esquineros) {
		this.esquineros = esquineros;
	}

}
