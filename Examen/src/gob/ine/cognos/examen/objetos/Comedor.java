package gob.ine.cognos.examen.objetos;

public class Comedor extends Mueble {
	public int nroSillas;

	public Comedor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Comedor(String codigo, String tipo_madera, double precio) {
		super(codigo, tipo_madera, precio);
		// TODO Auto-generated constructor stub
	}

	public Comedor(String codigo, String tipo_madera, double precio, int nroSillas) {
		super(codigo, tipo_madera, precio);
		// TODO Auto-generated constructor stub
		this.nroSillas = nroSillas;
	}

	public Comedor(int tipo,String codigo, String tipo_madera, double precio, int nroSillas) {
		super(tipo,codigo, tipo_madera, precio);
		// TODO Auto-generated constructor stub
		this.nroSillas = nroSillas;
	}

	public Comedor(int nroSillas) {
		super();
		this.nroSillas = nroSillas;
	}

	public int getNroSillas() {
		return nroSillas;
	}

	public void setNroSillas(int nroSillas) {
		this.nroSillas = nroSillas;
	}

}
