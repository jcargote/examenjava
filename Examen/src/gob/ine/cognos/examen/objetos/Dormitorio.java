package gob.ine.cognos.examen.objetos;

public class Dormitorio extends Mueble {

	public String tipoCama;

	public Dormitorio() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Dormitorio(String codigo, String tipo_madera, double precio) {
		super(codigo, tipo_madera, precio);
		// TODO Auto-generated constructor stub
	}

	public Dormitorio(int tipo,String codigo, String tipo_madera, double precio, String tipoCama) {
		super(tipo,codigo, tipo_madera, precio);
		// TODO Auto-generated constructor stub
		this.tipoCama = tipoCama;
	}

	public Dormitorio(String tipoCama) {
		super();
		this.tipoCama = tipoCama;
	}

	public String getTipoCama() {
		return tipoCama;
	}

	public void setTipoCama(String tipoCama) {
		this.tipoCama = tipoCama;
	}

}
